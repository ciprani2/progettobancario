import java.util.Date;
import java.util.Arraylist;
public class ContoCorrente {
private double saldo;
private boolean contoBloccato;
private boolean fidoAttivo;
private Arraylist<Movimento>movimenti;
private PersonaFisica intestatario;
  public ContoCorrente(double saldoIniz){
    this.saldo = saldoIniz;
    this.contoBloccato = false;
    this.fidoAttivo = fidoAttivo;
    this.movimenti = new Arraylist<>();
    this.intestatario = intestatario;
  }
public double getSaldo(){
  return saldo;
}
public boolean isContoBloccato(){
return contoBloccato;
}
public void bloccaConto(){
this.contoBloccato = true;
System.out.println("Il conto e' stato bloccato dall'autorita' giudiziaria.");
}
public void prelievo(double importoPrelievo){
if (!contoBloccato){
double importoMassimo = saldo;
if(fidoAttivo){
importoMassimo = Double.MAX_VALUE;
}
if (importoPrelievo > 0 && importoPrelievo <= importoMassimo){
Movimento movimento = new Movimento(new Date(), new Date(), importoPrelievo);
aggiungiMovimento(movimento);
saldo -= importoPrelievo;
System.out.println("Prelievo avvenuto con successo. Saldo rimanente: " + saldo);
} else {
System.out.println("Importo non valido o supera il limite massimo di prelievo.");
}
} else {
System.out.println("Operazione non consentita. Il conto e' bloccato.");
}
}
public void deposito(double importoDeposito){
if (importoDeposito > 0 && !contoBloccato){
Movimento movimento = new Movimento(new Date(), new Date(), importoDeposito);
aggiungiMovimento(movimento);
saldo += importoDeposito;
System.out.println("Deposito avvenuto con successo. Nuvov saldo: " + saldo);
}else if (contoBloccato){
System.out.println("Il conto e' stato bloccato. Impossibile effettuare depositi.");
}else{
System.out.println("Importo non valido per il deposito.");
}
}
public void stampaMovimenti(){
System.out.println("Movimenti del conto: ");
for(Movimento movimento : movimenti){
Movimento movimento = movimeti[i];
System.out.println("Data Richiesta: " + movimento.getDataRichiesta() + "| Descrizione: " + movimento.getDescrizione() + "| Tipologia: " + movimento.getTipologia() + "| Importo: " + movimento.getImporto());
}
}
public void stampaIntestatario(){
System.out.println("Intestatario del conto:");
System.out.println("Nome: " + intestatario.getNome());
System.out.println("Cognome: " + intestatario.getCognome());
System.out.println("Codice Fiscale: " + intestatario.getcodiceFiscale());
}
private void aggiungiMovimento(Movimento movimento){
movimenti.add(movimento);
}
  }


