import java.util.Scanner;
import java.util.Date;
public class TerminaleBanca {
  public static void main(String[] args) {
  Scanner scanner = new Scanner(System.in);
    ContoCorrente cSiric = new ContoCorrente(2500);
    System.out.println("Saldo: " + cSiric.getSaldo());

    System.out.println("Inserisci \n1.Per prelevare\n2. Per depositare");
    int scelta = scanner.nextInt();

    switch(scelta){
    case 1:
    if(cSiric.isContoBloccato()){
    System.out.println("Il conto e' bloccato impossibile effettuare prelievi. ");
    } else {
    System.out.println("Inserisci l'importo da prelevare: ");
    double importoPrelievo = scanner.nextDouble();
    Movimento prelievo = new Movimento(new Date(), new Date(), importoPrelievo);
    cSiric.prelievo(prelievo);
    }
    break;

    case 2:
    if(cSiric.isContoBloccato()){
    System.out.println("Il conto e' bloccato impossibile effettuare depositi ");
    }
    System.out.println("Inserisci l'importo da depositare: ");
    double importoDeposito = scanner.nextDouble();
    Movimento deposito = new Movimento(new Date(), new Date(), importoDeposito);
    break;

    default:
    System.out.println("Scelta non valida.");
    }
  }
}

